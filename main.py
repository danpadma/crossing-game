import pygame import random
from sys import exit

def scale_image(img, factor):
    size = round(img.get_width() * factor), round(img.get_height() * factor)
    return pygame.transform.scale(img, size)

def blitRotateCenter(surf, image, topleft, angle):
    rotated_image = pygame.transform.rotate(image, angle)
    new_rect = rotated_image.get_rect(center = image.get_rect(topleft = topleft).center)
    surf.blit(rotated_image, new_rect)

class Car():
    def __init__(self, image, xpos, ypos):
        self.car_img = scale_image(image, 1.5)
        self.xpos = xpos
        self.ypos = ypos

    def draw(self):
        blitRotateCenter(screen, self.car_img, (self.xpos, self.ypos), 90)

    def move(self):
        # move car vertically with speed = -2
        self.xpos -= 2
        self.respwan()

    def respwan(self):
        # if car out of screen
        if self.xpos < -50:
            # respawn
            self.car_img = scale_image(pygame.image.load(random.choice(car_images)), 1.5)
            # x 
            self.xpos = random.randint(screen_width, screen_width + 100)
            # y 
            self.ypos = random.randint(50, screen_height - 50)

class Player():
    def __init__(self):
        self.player_img = scale_image(pygame.image.load("img/frog.png"), 1)
        self.player_rect = self.player_img.get_rect(center=(screen_width//2, 480))
        self.speed = -10

    def draw(self):
        screen.blit(self.player_img, self.player_rect)

    def move(self):
        # respawn
        if self.player_rect.y < -10:
            self.player_rect.y = 480

pygame.init()
clock = pygame.time.Clock()

screen_width = 800
screen_height = 500
screen = pygame.display.set_mode((screen_width, screen_height))

# create player
frog = Player()

# create cars
car_images = [f"img/car_{index}.png" for index in range(8)]
car_total = 20
cars = []
for _ in range(car_total):
    car_img = pygame.image.load(random.choice(car_images))
    car_xpos = random.randint(0, screen_width)
    car_ypos = random.randint(50, screen_height - 50)
    new_car = Car(car_img, car_xpos, car_ypos)
    cars.append(new_car)

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                frog.player_rect.y += frog.speed
    
    screen.fill((64, 66, 88))

    for car in cars:
        car.draw()
        car.move()

    frog.draw()
    frog.move()

    pygame.display.update()
    clock.tick(60)

